//
//  StringHelper.swift
//  weather
//
//  Created by Florine BECQUET on 14/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func convertToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = dateFormatter.date(from: self) else {
            return nil
        }
        return date
    }
}

extension Double {
    func convertKelvinToCelsius() -> Double {
        return self - 273.15
    }
}

