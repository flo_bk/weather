//
//  DataHelper.swift
//  weather
//
//  Created by Florine BECQUET on 14/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import UIKit

class DateHelper {
    static func compare(date1: String, date2: String) -> Bool {
        let dateFromString1 = date1.convertToDate()
        let dateFromString2 = date2.convertToDate()
        
        if dateFromString1 == nil || dateFromString2 == nil {
            return false
        }
        
        //Compare day
        if Calendar.current.compare(dateFromString1!, to: dateFromString2!, toGranularity: .day) != .orderedSame {
            return false
        }
        
        //Compare month
        if Calendar.current.compare(dateFromString1!, to: dateFromString2!, toGranularity: .month) != .orderedSame {
            return false
        }
        
        //Compare year
        if Calendar.current.compare(dateFromString1!, to: dateFromString2!, toGranularity: .year) != .orderedSame {
            return false
        }
        
        return true
    }
}

extension DateFormatter {
    static let dateString: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE d MMMM"
        return formatter
    }()
    
    static let timeString: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
}

extension Date {
    func string(with format: DateFormatter) -> String {
        return format.string(from: self)
    }
    
    func localized(dateStyle: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        return dateFormatter.string(from: self)
    }
    
    func localized(dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
}
