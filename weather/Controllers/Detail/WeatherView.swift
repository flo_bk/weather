//
//  WeatherView.swift
//  weather
//
//  Created by Florine BECQUET on 14/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import UIKit

class WeatherView: UIView {
    init(weather: Weather) {
        super.init(frame: UIScreen.main.bounds);
        
        self.layer.cornerRadius = 10
        self.layer.backgroundColor = UIColor.gray.cgColor
        
        let lblDate = UILabel()
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        let date = weather.date?.convertToDate()
        lblDate.text = date?.string(with: .timeString)
        lblDate.textColor = UIColor.white
        self.addSubview(lblDate)
        
        let lblTemp = UILabel()
        lblTemp.translatesAutoresizingMaskIntoConstraints = false
        lblTemp.text = String.init(format: NSLocalizedString("temp", comment: ""), String(format: "%.2f", weather.temp.convertKelvinToCelsius()))
        lblTemp.textColor = UIColor.white
        self.addSubview(lblTemp)
        
        let lblRain = UILabel()
        lblRain.translatesAutoresizingMaskIntoConstraints = false
        lblRain.text = String.init(format: NSLocalizedString("rain", comment: ""), String(weather.rain))
        lblRain.textColor = UIColor.white
        self.addSubview(lblRain)
        
        let views = ["lblDate": lblDate, "lblTemp": lblTemp, "lblRain": lblRain]
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-[lblDate]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-[lblTemp]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-[lblRain]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[lblDate]-10-[lblTemp]-10-[lblRain]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
