//
//  ListViewController.swift
//  weather
//
//  Created by Florine BECQUET on 13/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

let defaultLatitude = 48.85341
let defaultLongitude = 2.3488

class ListViewController: UITableViewController, CLLocationManagerDelegate {
    var locationManager: CLLocationManager!
    var tableViewData: Array<Section>?
    var userLatitude = defaultLatitude
    var userLongitude = defaultLongitude
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("weather", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: NSNotification.Name("UpdateWeathersNotification"), object: nil)
    }
    
    @objc func update() {
        tableViewData = ModelService.getAllWeathers()
        self.tableView.reloadData()
    }
    
    func getAllWeathers() {
        let updated = WeatherService.getAllWeathers(latitude: userLatitude, longitude: userLongitude)
        if !updated {
            update()
        }
    }
    
    //MARK: UITableView
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData != nil ? tableViewData!.count : 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SimpleTableviewCell";
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell.init(style: UITableViewCellStyle.value1, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let date = tableViewData![indexPath.row].name.convertToDate()
        cell?.textLabel?.text = date?.string(with: .dateString)
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController(nibName: nil, bundle: nil)
        detailVC.items = tableViewData![indexPath.section].items
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    //MARK: CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        userLatitude = userLocation.coordinate.latitude
        userLongitude = userLocation.coordinate.longitude
        getAllWeathers()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
        getAllWeathers()
    }
}
