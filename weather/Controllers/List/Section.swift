//
//  Section.swift
//  weather
//
//  Created by Florine BECQUET on 14/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import UIKit

class Section {
    var name: String!
    var items: Array<Weather>!
    
    init(name: String, items: Array<Weather>) {
        self.name = name
        self.items = items
    }
}
