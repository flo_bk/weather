//
//  ModelService.swift
//  weather
//
//  Created by Florine BECQUET on 13/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ModelService {
    static func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
        
    static func managedObjectContext() -> NSManagedObjectContext {
        return ModelService.appDelegate().persistentContainer.viewContext
    }
    
    /**
     * Get the weather in coredata
     * Return Weather?
     */
    static func getWeatherForDate(date: String) -> Weather? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        fetchRequest.predicate = NSPredicate(format: "date == %@", date)
        
        do {
            // Execute Fetch Request
            let records = try ModelService.managedObjectContext().fetch(fetchRequest) as! [Weather]
            return records.count != 0 ? records.first : nil
        } catch {
            print("Unable to fetch managed objects for entity Weather.")
        }

        return nil
    }
    
    /**
     * Get all weathers sorted by date asc
     */
    static func getAllWeathers() -> Array<Section>? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        let sort = NSSortDescriptor(key: #keyPath(Weather.date), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        
        do {
            // Execute Fetch Request
            let records = try ModelService.managedObjectContext().fetch(fetchRequest) as! [Weather]
            var sections = Array<Section>()
            
            if records.count != 0 {
                var date = records.first?.date!
                var items = Array<Weather>()
                for weather in records {
                    items.append(weather)
                    if !(DateHelper.compare(date1: date!, date2: weather.date!)) {
                        sections.append(Section(name: date!, items: items))
                        date = weather.date!
                        items = Array<Weather>()
                    }
                }
            }
            return sections
        } catch {
            print("Unable to fetch managed objects for entity Weather.")
        }
        
        return nil
    }
    
    /**
     * Update weather in coredata
     */
    static func updateWeather(date: String?, temp: Double?, rain: Double?) {
        if date == nil || temp == nil || rain == nil {
            return
        }
        
        var weather = ModelService.getWeatherForDate(date: date!)
        if weather == nil {
            let entity = NSEntityDescription.entity(forEntityName: "Weather", in: ModelService.managedObjectContext())!
            weather = (NSManagedObject(entity: entity, insertInto: ModelService.managedObjectContext()) as! Weather)
        }
        
        weather?.setValue(date, forKey: "date")
        weather?.setValue(rain!, forKey: "rain")
        weather?.setValue(temp!, forKey: "temp")
        
        do {
            try ModelService.managedObjectContext().save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    /**
     * Remove weather in coredata
     */
    static func removeWeatherForDate(date: String) {
        let weather = ModelService.getWeatherForDate(date: date)
        if weather == nil {
            return
        }
        
        do {
            ModelService.managedObjectContext().delete(weather!)
            try ModelService.managedObjectContext().save()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
    }
}
