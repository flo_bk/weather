//
//  Services.swift
//  weather
//
//  Created by Florine BECQUET on 13/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherService {
    /**
     * Get all weathers by internet or coredata
     */
    static func getAllWeathers(latitude: Double, longitude: Double) -> Bool {
        if NetworkReachabilityManager()!.isReachable {
            WeatherService.callAPI(latitude: latitude, longitude: longitude)
            return true
        } else {
            return false
        }
    }
    
    /**
     * Call API weather
     */
    static func callAPI(latitude: Double, longitude: Double) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request("https://www.infoclimat.fr/public-api/gfs/json?_ll=\(latitude),\(longitude)&_auth=VU8FEgN9BCZSf1BnAXcBKFQ8DjtZL1B3BHgCYQxpBXhTOFY3VTVQNlA%2BBntQfwA2Ay4BYgE6BzcKYQB4WCoFZFU%2FBWkDaARjUj1QNQEuASpUeg5vWXlQdwRuAmcMfwVkUzRWN1UoUDNQPAZsUH4ANQM3AWMBIQcgCmgAYlg0BWNVPgVmA2YEZVI0UDMBLgEqVGIOP1ljUDkEYAJlDGMFYVMzVjtVMlBgUG4GZ1B%2BADcDMgFgATYHOQpgAG9YMgV5VSkFGAMTBHtSfVBwAWQBc1R6DjtZOFA8&_c=2b812961083c5c48c9de94c4a19ef549").responseJSON { (response) in
            do {
                let json = try JSON(data: response.data!)
                if json == JSON.null {
                } else {
                    for (date, weather) in json {
                        let temps = weather["temperature"].dictionary
                        if temps != nil {
                            ModelService.updateWeather(date: date, temp: temps!["sol"]!.double, rain: weather["pluie"].double)
                        }
                    }
                    NotificationCenter.default.post(name: Notification.Name("UpdateWeathersNotification"), object: nil)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            } catch {
                print("error")
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
}
