//
//  weatherTests.swift
//  weatherTests
//
//  Created by Florine BECQUET on 13/01/2018.
//  Copyright © 2018 flo.bk. All rights reserved.
//

import XCTest
@testable import weather

class weatherTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        ModelService.updateWeather(date: "2018-01-15 10:00:00", temp: 283.3, rain: 2.8)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let weather = ModelService.getWeatherForDate(date: "2018-01-15 10:00:00")
        XCTAssertNotNil(weather)
        ModelService.removeWeatherForDate(date: (weather?.date!)!)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
